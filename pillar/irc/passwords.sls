#!yaml|gpg

passwords:
  mibbit: |
    -----BEGIN PGP MESSAGE-----
    Version: GnuPG v2.0.14 (GNU/Linux)

    hQIMA41nn/5kar90ARAAhUMlC7pb9CXuHic48H2lKaNXhSropv8Y9D5/iS4INTF6
    zdXG8zFzkOovkK6a2AJvriEsg3iNYETQQHzcNAM12RMsRRai6KGN54Gk0/FQtjWa
    d/HmgHlDwkK14KsUYR/OmZrc8kL7po4x9GDq6RphsbPof46T7T1UIGj/hp6k31oA
    imM2pCAkGUyjY/VX+0ub/qx9ecnBBZ9N8E15pQKtM5cRCHaG281JJGixkEltaQDu
    kArjwVB2n8y/8xLR1FvGj1H76HZD73Bn7i/MmcmpRzfkOI8TqIZgp98jcoN95fUx
    oT5JUu7OEibSxSdqMTLANA4EWKFxJtPoOUh9kVFq381ca3oVC+Th8sPDRahZ0kOa
    vuC4kFV0ATL3QwZlqQSbThpC6sXgtRlIvjMzrGXT+0uno+u5uawgyvDyUPs2ihrx
    1Hxw3Ajnh8M6QbRatZ8gGdrKwyZ5GImn7/xMiKLzjfhplelJhji2+W7/aBIxNi83
    zwpIYDKucI9sYMb8XZ5+qeaeF8CMtcRjfBk+trbom3BOxTnvxKVkydyLrkaShRFd
    KTPe8IfnXlHY+ucyCYKy3kIpicw9BXM+/iJjf1jXguoQ6zs/iSY8YlMAPKqXRTtu
    3rOgnkzUu0j/O3O6M1pYa60wynhZgM5N3ZHsX1prK/qnQi95rl8/6ntE40A3sprS
    TgE7Mn11wkS2lQO6V7wR+o+M5mQtx9FoKY6HKXbphXAQPg2Sb6iAaEdxlf3axc2O
    KgiOEipCnknRMdOk21TNymTTcEI5r2FCdbIywMXhRg==
    =+xfV
    -----END PGP MESSAGE-----
  kiwi: |
    -----BEGIN PGP MESSAGE-----
    Version: GnuPG v1
    Comment: GPGTools - http://gpgtools.org

    hQIMA41nn/5kar90AQ//TcDJt4n8/zc0iljR5f4NBUdh/RprUURoA8o09TwgNaOq
    byqw1Nb5P+Q6/08RwNHr7l0dFeR4dzqSPXiV4WC9MuZ88Obs6oVrm5Xn6UX5EVUr
    0OCB7nxhZMkGjNooWMVhotZ7zWKu1ILCyaik0g2IBgp88LgIp/QksXxTBIZyQr81
    ZtBRdBbwfuuklA/1HG3ZzODfKr61830FETy1V8G45QFXavjE3rnZF2rgOhHLp8yj
    5GiR6Vqa+uxJmG6B57Wu6ZyMGcBl3cHb2uglFoCax4wk4JLyc/TGs/gnJBcZ1+Wb
    1T2Py9oL+1zrhg0vV0Xpxsk2LcC9ekVw8iR7U+qhbbWxR5EwDDXBBvgVptxmu4JS
    V3LLffKivvL66gl6AUUfYutT3mFjO7dW+oJH4Wp79D9dB4gziTF2Dghxj9iKGHmo
    cmccbjJmC7b9EDhgao5Xo2VtTmVRlVJ+CCoYF5KaZEOqdniDnZWt63HalF9ZwBfX
    yYgsediuyw3ETtDFWmyk23/GfqBobsxrIMb3vnCC4blBdrIcXTZkpJRsl6bK7rKX
    hqeQArFIir/5XqeeqIOXd4nl05exd/ZCJHTibfiiUGWzGZZJjXI5ERNyYhpRBF4y
    1wI4Vwfipz3z9BaGCoI6B7nfIeeWNcbK2dEs40TSh7TSdgfOKyh6aaXPYduxc5nS
    RQEYOw/JYVS64bo9FydPn2lZ4c2/fG3/aXG41GeEczeM5R0wJvPTnCpx2HqDpat0
    CakVNSfNTkFljpefUkreGZvPhZe6hQ==
    =+WkU
    -----END PGP MESSAGE-----
  cloak_key: |
    -----BEGIN PGP MESSAGE-----
    Version: GnuPG v2

    hQIMA41nn/5kar90ARAAqOQdkM557AVdJxkhT1XtpiOeutQySd6usfzkwmXwySHq
    rXDDcC+PsSRH56ySHZ+hthOzzzUg9RiRdl24CdrrTwvzR0eKeA7tRoCvqO4sy73V
    mvZaE1cL1EtEqtPnMhMtxzb2W5ak2rHOgNzWIJ6+FOWDE6zDGd6ykvvCVXyxkDTD
    SNBKYnkS0fkNP/CKoLMT3sOFktvjePObMHJNO8urMz//7SBPi3DlhKjT2k+PHsgh
    Ia03bODHmAbW0dnv+p51ZTHJOqRJZKNIYXUlV5rbenlaQLKuEyTGkx3UdTT4UeVT
    8wOegsUYppnbWPoUke4Oepqf718Dj6eyiglMIAsQPeQbrA39/N0kXWoN2nRq0BR0
    2FNUK2Hq9FSCIInNITzSLIkMlmNAksr3C4yHYkpvI2g2VywjSxnSX4hl37cQMNOT
    qCalQRRTW0BLd53Xt/hsAasYVIMoOSz13QEZwGycdESijzjK4BOc07AjNp+7GAdA
    Z8knC7b0B4vJPPw9giKy/Be5svjey1cRGp4/I4nAILvlQU/2FIJ7DKRNN4gbHHjp
    ERzNb9FPscsixk8R6+cc83vPKFUL3ppl+TZeHfrL3xFaPntDhY/cKUZGlsBFsBKm
    O9qkEDsw/dslyU2JxTtvUvq9WSctYgHslQjfjqECqt7TTjTj4a/Dgf8A/QYMKsTS
    RgHO3H0h82dcCe824cuWzhIFSCErJqGORX/nRu1Op0KG6eICmccXTDyR28YY6KzF
    5GoOlyrGnYXIv8vMfT+FY/cJ0geEXzo=
    =Nved
    -----END PGP MESSAGE-----

links:
  celadon: |
    -----BEGIN PGP MESSAGE-----
    Version: GnuPG v2.0.14 (GNU/Linux)

    hQIMA41nn/5kar90ARAAhjCrxu+8wa0rFNuWTE06q7WO3XRuF5Xyu10OnQfVB8Zm
    nTtjK+68XaZkwUak6DHr+QCnh3xN4c1J9sWGdv/TVU1/PwhHrDpI4TbCUqUh8ASo
    pDxl2YUTi/Pp69g6eexaQb0Sc5UKJ/fiqzppm69H9U2saSKlRtlab7J1sGO56ovh
    nHFVL5xKA9882pZ5BBApTrEAGLlObJOYpIYehade8wtj1sJ3/uOPpp+VcVaCsHy7
    rSgS6Ubgen4qtL4hoddSdV8y6GgPuS/q8kwM2srLaqZSKTn+8UdjwAIYhdwgjrjv
    rKTG9/h8AlsGRoXe9HN1BTCbsCV5O4CYNVxHQZ5ctWtLFJKItOoH42rnTYtoHBap
    cYdUrElJEi5I3SUKMkSp8vQ4Hb94WCTSp158kzbi0kfX5UlILBIM1P6XcobBKsvx
    N2DD5e0uBzb3+RI/1piSa1DuZTwnJ50137dyqSiLRuX8ytLwO1iVZJWuBelmYyio
    5uYN8Sopsr75KA3WHYGeRNae1xDVKw3uBX46Z709sPBgwAI9RK18Ijdd0pdaXiVD
    k0YfQf3fLD1sWZotPMSBCVKQhCwS9ERyGQTDfqU8ByRTF12O8KJWDa8BSyxJKg4A
    YuqT7zaLybmHYGJIdkcxMjcvEHj2knrqUhuQiB35Ic+WBY/jILyiF87mti5TQTDS
    WwGf/0afoPh//yhyUJqSFpgbR+kjJiQrpCAXp8239gCbi2Vo9IbZPPQQncBmwzPw
    XMxCBVYMPxuqlvKxTkGd49saxd993NVShc+s1lqzVdkDMIFnnhqIVTtSvCM=
    =U8my
    -----END PGP MESSAGE-----
  cthulhu: |
    -----BEGIN PGP MESSAGE-----
    Version: GnuPG v2.0.14 (GNU/Linux)

    hQIMA41nn/5kar90AQ//ehLQo8LKF5WiVw9Rsrw5CyKZ/Xem1JYlW3JzIgTeAe21
    /nrxZu1V6duPlsc5XqMkMGVBtWDidC+ql5aFI41Eb40wF7ngV9KpXRREA0TkOW0L
    DbVd7MBhsUPAEFcpGSnUtTeQkr6Q4FDK6wWOrMzfXysfzsHbW6eks04EcCz/7u+X
    MVpoekql6LZaISvQvDUaJq9QNoPOVyp60weh+AO3/aWsoMo15kDFaze4i64vAuXi
    XCS0GA2BPL2E4QpBeFSJ8IkFHfa/mTJKhThGapAU8sOC8WcLQH24RcWkULp3TbZf
    dMXTEV5aAehuCvdPlvYaR6ivj/VOCE+V86Ul8YaNPFsGrKqf9Ga2yHHT3Jtd6ChH
    wP6say1r1Kwyd3yQS7dWzSVZJ8PaxyfpdtRJ5aDawOW2/hr8fbXiZ+2FipgK3+ut
    hi39LSddNnh7KdlI2Tgp4h5BOhas7gq2l2SBj55mn+vN/a8w+lqKAnmpHvB7IWOj
    QiJZuTheyW+nBXKMTTSeq9i1m0ZtPdkDvqiN3kMWPuGikeG9Nv3O7Df1aWGqzquT
    s770jwMdrHLjkN3Moj5vVRZeJg1f9CJld3VssBH8CM06n9wRASeve71yHBBEGvAA
    ghZCz3P19mJmSn592aYBoTbuEBHs6GSmk9F4HwXKGXowmTFERzBp+MVA0DdRaVTS
    WwHS9JrNdIMxdMd2vIVHu8A9p8diBNiyfqcsEAt4vmCGy4wfG+Y15+SY0jf6rwV5
    oHRX3MeOp0AEQQzAS8wF6MJ54AV3bmgP9O0G0OUkH7DnUsIWRw5l18qjHhg=
    =2lxF
    -----END PGP MESSAGE-----
  zeta: | 
    -----BEGIN PGP MESSAGE-----
    Version: GnuPG v2

    hQIMA41nn/5kar90AQ/8DVxQAOqYklYOxwWLUngB3nU/HwV9RlVpUtWVqqMsVgQV
    hW/J1J3t/k5z5g6ykG2A3LHu8ys4Sv7N2tjhlZU3+nNL5NaqKhM9hjszwCgm2z8k
    djmKqfJpMhN2nicvhPG2Lq+9U06NGNdS32jlo8+uT1oMPXM8dRlGzUPVYDLyFG1n
    L0SA22qnqq1eWpLikrwbXxdGiEynrxotYhd2GQSmuIFSpP5XyuSTqnBm4zSTzTR+
    YwFrgPbJAhq0z4t87q2Zzhw7tHTyQvS1BKOT5fwj5qO5YQVFz5OZnyOeRaP0s/hl
    bwu2bFc9r3dA/R4EOduC6BsJwgt5dZc3QqgDA0LHLebGT05RKttAaUdLjY1ZqVoD
    AfX9xJcYTv0TMl0BIwvyxCl7lgwuin5SegLJjnnBUPLoy9iCCfyDpX+QB2l+E7Om
    xZnT5bvhj/q5LFKydC50FEAcj/M1uCrUGbxJbOn/kkYjVsr6626AtopGDFx6gdq9
    iUWVK/kJugoIzsdrD7/xumpvAiWErpwBs0yfGKbyESLPKaRvUyHXbxPEFHuo7god
    BsSEwYvP17k63GfEQK+VwtXHUcaacqNNpEyLR5W/kCLMOjkUxS+LwmSHBBvWwctu
    ihzRiDVtVP+elAliHdverNMB+xMWekngR+jx1qpQNIuTAq3fEGLB08Y6Fj2UPl7S
    cQFHmN8epOSJV7tm3Isa20rU11Kw8O4tMR6K1e9xU3AV6dW+9ltnkIRb5qnjv0dQ
    Hf7bm9c33gRrybkVV3dGNsoEH4qyll23QGRFH1hxalZKHaf3Hdy2TfTD2yubYp1+
    u0FfGNXuQIp+eO7TraYbigcW
    =fUGL
    -----END PGP MESSAGE-----
