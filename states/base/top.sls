base:
  '*':
    - shells
    - python
    - users
    - edit.vim
    - security
  'E@^(celadon|cinnabar|cthulhu|indigo|lavender|saffron|verus).darkscience.net$':
    - nginx
  'irc-*':
    - inspircd
  'pallet.darchoods.net':
    - nginx
  'G@os:FreeBSD':
    - security.freebsd
  'G@os:Debian':
    - security.debian
