{% for usr in 'dijit','linky','elric','derecho','narada','liothen','sebkinne','kyle' %}
  /home/{{usr}}/.ssh/authorized_keys:
    file.managed:
      - source: salt://users/keys/{{usr}}
      - makedirs: True
      - mode: 600
      - user: {{usr}}
      - group: {{usr}}
      - require:
        - user: {{usr}}
{% endfor %}

