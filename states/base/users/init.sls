{% set path = '/usr/local' %}
{% if grains['os'] != 'FreeBSD' %}
{{pillar.get('admin_group', 'systems')}}:
  group.present:
    - gid: 305
{% set path = '' %}
{% endif %}

{% for usr in 'dijit','kyle','derecho','elric','linky','narada','liothen','sebkinne' %}
{{ usr }}:
  group:
    - present
  user:
    - present
    - groups: [{{pillar.get('admin_group', 'systems')}},{{ usr }}]
    - empty_password: True
    - enforce_password: False
    - shell: {{path}}/bin/zsh
  file.managed:
    - name: /home/{{ usr }}/.zshrc
    - source: salt://shells/files/zshrc
    - require:
      - user: {{ usr }}
{% endfor %}

kylef:
  user.absent: []

include:
  - .sshkeys

sudo:
  pkg.installed

{{path}}/etc/sudoers:
  file.managed:
    - template: jinja
    - source: salt://users/sudoers
