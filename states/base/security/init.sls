{% set sshd_name = 'sshd' %}
{% if grains.os == 'Debian' %}
{% set sshd_name = 'openssh-server' %}
{% endif %}

/etc/ssh/sshd_config:
  file.managed:
    - source: salt://security/files/sshd
    - user: root
    - mode: 0600

{{sshd_name}}:
  pkg.latest: []
  service.running:
    - watch: 
      - /etc/ssh/sshd_config
    - reload: True

